# JavaBeans for swing by Neo Cs (neo-cs-beans)
This project is a swing library. And allows get some additional features for 
swing components like background image on JPanel, vertical text orientation.

## Installation
### Requirements
This project require:
* [JDK 1.8](http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html)
* [Maven](https://maven.apache.org)
### Building from sources
This project is ready for compile using Maven.
- Compile with `mvn clean install`

## License
Please refer to file [LICENSE](LICENSE.md), available in this repository.

