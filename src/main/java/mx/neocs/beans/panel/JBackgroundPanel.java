/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.beans.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.Serializable;

import javax.swing.JPanel;

/**
 * This class extends from JPanel and allows it an alpha value and borders
 * radius.
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 * @version 3.4.1 28/02/09
 * @since 1.0.0
 */
public class JBackgroundPanel extends JPanel implements Serializable {

    /** The Serial version UID. */
    private static final long serialVersionUID = 9162897719101025868L;
    /** Property name for alpha. */
    public static final String PROP_ALPHA = "alpha";
    /** Property name for border radius. */
    public static final String PROP_RADIUS = "radius";
    /** Alpha allows values 0 &lt; alpha &lt; 255. */
    protected int alpha = 100;
    /** The value for the borders radius. */
    protected int radius = 15;

    /**
     * Get the value of borders radius.
     *
     * @return the value of radius.
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Set the value of borders radius.
     *
     * @param radius new value of radius.
     */
    public void setRadius(int radius) {
        final int oldRadius = this.radius;
        this.radius = radius;
        firePropertyChange(PROP_RADIUS, oldRadius, radius);
    }

    /**
     * Get the value of alpha.
     *
     * @return the alpha level.
     */
    public int getAlpha() {
        return alpha;
    }

    /**
     * Set the value for alpha.
     *
     * @param newAlpha the alpha to set.
     */
    public void setAlpha(int newAlpha) {
        final int oldAlpha = getAlpha();
        alpha = (newAlpha >= 0) && (newAlpha <= 255)
                ? newAlpha : (newAlpha > 255) ? 255 : 0;
        firePropertyChange(PROP_ALPHA, oldAlpha, alpha);
    }

    @Override
    public void paintComponent(Graphics g) {
        final Dimension size = getSize();
        final Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(
                new Color(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue(), getAlpha()));
        g2.fillRoundRect(0, 0, size.width, size.height, getRadius(), getRadius());
        setOpaque(false);

        super.paintComponent(g2);
    }
}
