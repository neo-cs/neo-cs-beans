/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.beans.panel;

import java.net.URL;

import javax.swing.ImageIcon;

/**
 * This enum contains all the images for the beans.
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 * @version 1.0.0
 * @since 1.0.2
 */
public enum ImageConstants {
    
    DEFAULT_BACKGROUND_IMAGE("mx/neocs/beans/panel/images/fondo.jpg");
    
    /**
     * Get an image as ImageIcon.
     * @return an image. 
     */
    public ImageIcon getImageIcon() {
        final URL resource = ImageConstants.class.getClassLoader().getResource(url);
        return new ImageIcon(resource);
    }

    /**
     * Create a new instance for the default images for the beans.
     * 
     * @param url the URL where the image is.
     */
    private ImageConstants(String url) {
        this.url = url;
    }

    private final String url;
}
