/*
 * Copyright (C) 2017 Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package mx.neocs.beans.panel;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.Serializable;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * This class extends from JPanel and allows set a background image for this.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 * @version 2.5.1 28/02/09
 * @since 1.0.0
 */
public class JBackgroundImagePanel extends JPanel implements Serializable {

    /** The Serial version UID. */
    private static final long serialVersionUID = -4930005072823213312L;
    /** Property name for background image icon. */
    public static final String BACKGROUND_IMAGE_ICON = "backgroundImageIcon";
    private Icon backgroundImageIcon;

    /**
     * Create a new instance from JBackgroundImagePanel setting a background image.
     */
    public JBackgroundImagePanel() {
        backgroundImageIcon = ImageConstants.DEFAULT_BACKGROUND_IMAGE.getImageIcon();
    }

    @Override
    public void paintComponent(Graphics g) {
        Dimension dimension = getSize();
        ImageIcon imagenFondo = (ImageIcon) getBackgroundImageIcon();
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawImage(imagenFondo.getImage(), 0, 0, dimension.width, dimension.height, null);
        setOpaque(false);

        super.paintComponent(g2);
    }

    /**
     * Get the value of background image.
     * 
     * @return the backgroundImageIcon.
     */
    public Icon getBackgroundImageIcon() {
        return backgroundImageIcon;
    }

    /**
     * Set the value for the background image.
     * 
     * @param newBackgroundImageIcon the backgroundImageIcon to set.
     */
    public void setBackgroundImageIcon(Icon newBackgroundImageIcon) {
        final Icon oldValue = backgroundImageIcon;
        backgroundImageIcon = newBackgroundImageIcon;

        firePropertyChange(BACKGROUND_IMAGE_ICON, oldValue, backgroundImageIcon);
        updateUI();
    }
}
